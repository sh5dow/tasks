const { cart, requiredCurrencies } = require('./shoppingCartData');
const pricing = require('./pricing');
/**
 * function to simulate one component of bigger project
 */
async function runCalculation() {
  const result = await pricing.calculateCart(cart, requiredCurrencies);
  console.log('Shopping cart: ');
  console.log(result);
}

runCalculation();


