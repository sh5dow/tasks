module.exports = {
  cart: [
    { price: 20 },
    { price: 45 },
    { price: 67 },
    { price: 1305 }
  ],
  requiredCurrencies: ['EUR', 'GBP', 'RUB', 'JPY']
};