const fetch = require('node-fetch');
const config = require('./config');

/**
 * Vallidates input for shopping cart
 * @param {Array} cart 
 * @param {Array} requiredCurrencies 
 */
const validateInput = (cart = [], requiredCurrencies = []) => {
  //validate if its array
  if (!Array.isArray(cart)) {
    throw new Error('Input variable cart is not of type array');
  }

  if (!Array.isArray(requiredCurrencies)) {
    throw new Error('Input variable requiredCurrencies is not of type array');
  }

  //validation for all objects in array
  if (cart.some(e => typeof e.price !== 'number' || e.price === null)) {
    throw new Error('Not all input "cart" objects are valid');
  }

  //validation for all objects in array
  if (requiredCurrencies.some(e => typeof e !== 'string')) {
    throw new Error('Not all input "requiredCurrencies" objects are valid, encountered different type from number');
  }
}

/**
 * Calculates shopping cart in different currencies
 * @param {Array} cart 
 * @param {Array} requiredCurrencies 
 */
const calculateCart = async (cart = [], requiredCurrencies = []) => {

  validateInput(cart, requiredCurrencies);

  const totalPriceUSD = cart.reduce((price, b) => price + b.price, 0);
  const currencyExchange = await loadCurrencyExchangeRate(requiredCurrencies);

  const totalPriceInDiffCurrencies = {
    'USD': totalPriceUSD
  };

  if (currencyExchange && currencyExchange.rates) {
    Object.keys(currencyExchange.rates).forEach(item =>
      totalPriceInDiffCurrencies[item] = totalPriceUSD * currencyExchange.rates[item]
    )
  }

  return totalPriceInDiffCurrencies;
}

/**
 * loads data from web service
 * @param {*} requiredCurrencies 
 */
const loadCurrencyExchangeRate = async (requiredCurrencies = []) => {
  const exchangeURL = `${config.serverURL}latest?base=USD&symbols=${[...requiredCurrencies]}`;
  let currencyExchange = null;
  await fetch(exchangeURL)
    .then(res => {
      if (res.status !== 200) throw new Error('Error contacting currency exchange service')
      return res.json();
    })
    .then(json => currencyExchange = json);
  return currencyExchange;
}


module.exports = {
  calculateCart: calculateCart
}