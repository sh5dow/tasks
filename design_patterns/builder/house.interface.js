class HouseInterface {
    constructor() {
        this.buildFoundation = null;
        this.buildStructure = null;
        this.buildRoof = null;
        this.paintHouse = null;
        this.furnishHouse = null;
        this.getHouse = null;
    }
}

module.exports = {
    HouseInterface
}
