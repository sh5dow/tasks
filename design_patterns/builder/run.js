const { ConcreteHouse } = require('./concreteHouse');
const { Engineer } = require('./engineer');

const type = new ConcreteHouse();
const eng = new Engineer(type);

eng.constructHouse();
console.log(eng.getHouse());

