const { HouseInterface } = require('./house.interface');
const { House } = require('./house');

class ConcreteHouse extends HouseInterface {
    constructor() {
        super();
        this.house = new House();
    }

    buildFoundation = () => {
        this.house.setFoundation("Concrete, brick, and stone")
    }

    buildStructure = () => {
        this.house.setStructure('Structure..');
    }

    buildRoof = () => {
        this.house.setRoof('roof...');
    }

    paintHouse = () => {
        this.house.painted = true;
    }

    furnishHouse = () => {
        this.house.furnished = true;
    }

    getHouse = () => {
        return this.house;
    }
}

module.exports = {
    ConcreteHouse
}
