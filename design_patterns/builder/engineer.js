class Engineer {
    houseBuilder = null;

    constructor(houseBuilder) {
        this.houseBuilder = houseBuilder;
    }

    constructHouse() {
        this.houseBuilder.buildFoundation();
        this.houseBuilder.buildStructure();
        this.houseBuilder.buildRoof();
        this.houseBuilder.paintHouse();
        this.houseBuilder.furnishHouse();
    }

    getHouse() {
        return this.houseBuilder.getHouse();
    }
}

module.exports = {
    Engineer
}
