 class House {

    foundation;
    structure;
    roof;
    furnished;
    painted;

    constructor() {

    }

    setFoundation = (foundation) => {
        this.foundation = foundation;
    }
    setStructure = (structure) => {
        this.structure = structure;
    }
    setRoof = (roof) => {
        this.roof = roof;
    }
    setFurnished = (furnished) => {
        this.furnished = furnished;
    }

    setPainted = (painted) => {
        this.painted = painted;
    }

}

module.exports = {
    House
}
