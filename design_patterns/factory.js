class FactoryUser {
    constructor(firstName, lastName, isEngaged = false) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.engaged = isEngaged;
    };

    get getFirstName() {
        return this.firstName;
    }

    get getLastName() {
        return this.lastName;
    }

    get isEngaged() {
        return this.engaged;
    }

    get fullName() {
        return `${this.getFirstName} ${this.getLastName}`;
    }

    set engagedStatus(status) {
        this.engaged = status;
    }
}

const user1 = new FactoryUser('Daniel', 'Javorsky');
const user2 = new FactoryUser('Tina', 'Javorska');

console.log('user1: ', user1);
console.log('user2: ', user2);

user2.engagedStatus = true;

console.log('user1: ', user1);
console.log('user2: ', user2);
