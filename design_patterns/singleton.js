var singletonInstance = null;

class Sigleton {
    constructor(firstName, lastName) {

        if(singletonInstance) {
            console.log('Singleton already exists');
            return singletonInstance;
        } else {
            this.firstName = firstName;
            this.lastName = lastName;
            singletonInstance = this;
        }
    };

    get getFirstName() {
        return this.firstName;
    }

    get getLastName() {
        return this.lastName;
    }

    get isEngaged() {
        return this.engaged;
    }

    get fullName() {
        return `${this.getFirstName} ${this.getLastName}`;
    }

    set setLastName(name) {
        this.lastName = name;
    }
}

const user1 = new Sigleton('God', 'Allmighty');
const user2 = new Sigleton('Tina', 'Javorska');

console.log('user1: ', user1);
console.log('user2: ', user2);

user2.setLastName = 'Allmightier';

console.log('user1: ', user1);
console.log('user2: ', user2);
