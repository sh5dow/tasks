# Interview tasks

## Task1: Currencies for shopping cart

Implemented in node.js, can be run by issuing following commands in task1 directory:
```
npm install
node app.js
```

Uses public currency exchange web server to download required currencies, to add more currencies, adjust variable in:
```
shoppingCartData.js
```

with required currencies

## Task2: Login Page with BEM

Implemented using plain html & CSS with BEM syntax.

Run by clicking on index.html
